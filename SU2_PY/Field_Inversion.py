from plot_features import *
import subprocess
import os
import sys
import numpy as np
import copy

def run(nprocs):
	subprocess.call("mpirun -np "+nprocs+" SU2_CFD config_CFD.cfg > ../outputs/log_direct.out", shell=True)
	subprocess.call("cp nozzle.dat solution_flow.dat", shell=True)
	subprocess.call("mpirun -np "+nprocs+" SU2_CFD_AD config_CFD_AD.cfg > ../outputs/log_adjoint.out", shell=True)
	obj_fn   = np.loadtxt("obj_fn.dat")
	sys.stdout.write("Objective Function "+str(obj_fn)+"\t")
	return obj_fn

def read_beta_grad():
	gradient = np.loadtxt("beta_grad.dat")
	gradient = gradient[:,1]
	return gradient

def advance_beta(beta, gradient, step=0.1):
	max_grad = max(abs(gradient))
	beta_new = beta - step*gradient/max_grad
	sys.stdout.write("Maximum Gradient "+str(max_grad)+"\n")
	sys.stdout.flush()
	return beta_new

def write_beta(beta,n):
	np.savetxt("beta_turb_prod.dat", beta)
	np.savetxt("../beta_turb_prod/beta_turb_prod_"+str(n+1)+".dat", beta)

if __name__ == "__main__":
	
	subprocess.call("mkdir -p beta_turb_prod", shell=True)
	subprocess.call("mkdir -p temp", shell=True)
	subprocess.call("mkdir -p outputs", shell=True)
	os.chdir("./temp")
	subprocess.call("cp ../* ./", shell=True)
	
	start_iter = int(sys.argv[1])
	niter      = int(sys.argv[2])
	step       = float(sys.argv[3])
	nprocs	   = sys.argv[4]
	obj_func_ref   = 0.0
	obj_func_prev2 = 1000.0
	obj_func_prev  = 999.9
	obj_func       = 0.0

	flag_conv      = False
	
	if (start_iter>0):
		obj_func_ref = np.loadtxt("../beta_turb_prod/obj_fn_0.dat")
		subprocess.call("cp ../beta_turb_prod/beta_turb_prod_"+str(start_iter)+".dat ./beta_turb_prod.dat", shell=True)
	
	for it in range(niter):
		itr = start_iter + it
		sys.stdout.write("Iteration "+str(itr)+"\t")
		obj_func = run(nprocs)
		if(itr==0):
			obj_func_ref = copy.deepcopy(obj_func)
		gradient = read_beta_grad()
		
		if(itr==0):
			beta = np.ones_like(gradient)
		else:
			beta = np.loadtxt("beta_turb_prod.dat")
		
		beta = advance_beta(beta, gradient, step)

		if((obj_func_prev-obj_func)/abs(obj_func_prev2-obj_func_prev)<0.5):
			step = step/2.0

		obj_func_prev  = copy.deepcopy(obj_func)
		obj_func_prev2 = copy.deepcopy(obj_func_prev)
		
		write_beta(beta, itr)
		
		subprocess.call("cp ./obj_fn.dat ../beta_turb_prod/obj_fn_"+str(itr)+".dat", shell=True)

		subprocess.call("cp sample_features.dat ../outputs/sample_features_"+str(itr)+".dat", shell=True)
		subprocess.call("cp friction_val.dat ../outputs/friction_val_"+str(itr)+".dat", shell=True)

		subprocess.call("cp sample_features.dat ../outputs", shell=True)
		subprocess.call("cp friction_val.dat ../outputs", shell=True)

		if(itr==0):
			subprocess.call("mpirun -np "+nprocs+" SU2_SOL config_CFD.cfg", shell=True)
			subprocess.call("cp flow.vtk ../outputs/", shell=True)

		if(obj_func/obj_func_ref<0.05):
			sys.stdout.write("Converged!!\n")
			sys.stdout.flush()
			flag_conv = True
			break

	if(flag_conv==False):
		sys.stdout.write("Not Converged:\t Percent Reduction in Objective = "+str(1.0-obj_func/obj_func_ref)+"\n")
		sys.stdout.flush()

	sys.stdout.write("Writing features file")
	sys.stdout.flush()

	os.chdir("../outputs")

	postprocess()

	subprocess.call("rm -rf log_*", shell=True)
	
	os.chdir("../")

	subprocess.call("rm -rf temp", shell=True)
