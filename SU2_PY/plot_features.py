import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import copy
import subprocess

def filter(filt=False, synthetic=True):
	friction = np.loadtxt("./outputs/friction_val_baseline.dat", skiprows=1)
	
	friction_new = friction[friction[:,0].astype(int).argsort(),0:6]

	cf = copy.copy(friction_new[:,5])
	coords = friction_new[:,1:4]
	
	if(filt):
		spectrum = np.fft.fft(cf)
		spectrum[99:-100] = 0.0
		cf_filt = np.fft.ifft(spectrum)
	else:
		cf_filt = copy.copy(cf)

	x = np.linspace(0,2*np.pi,len(cf_filt))	
	syn_cf = np.abs(cf_filt + 0.01*(0.125*np.sin(x) + 0.25*np.sin(3*x) + 0.5*np.sin(6*x) + np.sin(9*x)))
	
	friction_new[:,5] = syn_cf
	if(synthetic):
		np.savetxt("TargetCf.dat", friction_new, fmt=['%u','%.18e','%.18e','%.18e','%.18e','%.18e'])
	
	plt.plot(cf)
	if(filt):
		plt.plot(cf_filt, linewidth=4)
	if(synthetic):
		plt.plot(syn_cf, linewidth=4)
	plt.show()
	fig = plt.figure()
	ax  = plt.axes(projection='3d')
	ax.plot(coords[:,0], coords[:,1], coords[:,2])
	plt.show()

def postprocess():
	subprocess.call("cp flow.vtk flow_aug.vtk", shell=True)
	
	features = np.loadtxt("sample_features.dat", skiprows=1)
	friction = np.loadtxt("friction_val.dat", skiprows=1)
	
	temp_arr = copy.deepcopy(features)
	
	features[features[:,0].astype(int),:] = temp_arr[:,:]
	
	tauwall  = np.zeros((len(features)))
	temp_tau = np.zeros((int(max(friction[:,-2])+1), int(max(friction[:,-1])+1)))
	for i in range(len(friction)):
		temp_tau[int(friction[i,-2]), int(friction[i,-1])] = friction[i,4]
	for i in range(len(features)):
		tauwall[i] = temp_tau[int(features[i,-2]), int(features[i,-1])]
	tauwall[tauwall<1e-10] = 1e-10
	
	ML_features = np.zeros((len(features), 4))
	ML_features[:,0] = features[:,0]
	ML_features[:,1] = features[:,2]*features[:,5]*features[:,1]*features[:,1]/features[:,3]
	ML_features[:,2] = features[:,4]*features[:,5]/tauwall
	ML_features[:,3] = features[:,1]
	
	np.savetxt("ML_features.dat", ML_features, fmt=['%u', '%18e', '%18e', '%18e'])
	
	print features[1:100,0]
	
	f = open("flow_aug.vtk",'a+')
	
	f.write("\nSCALARS ML_WallDist double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(features[i,1])+"\t")
	
	f.write("\nSCALARS ML_Density double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(features[i,2])+"\t")
	
	f.write("\nSCALARS ML_muL double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(features[i,3])+"\t")
	
	f.write("\nSCALARS ML_muT double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(features[i,4])+"\t")
	
	f.write("\nSCALARS ML_StrainMag double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(features[i,5])+"\t")
	
	f.write("\nSCALARS ML_ClosestWallShear double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(tauwall[i])+"\t")
	
	f.write("\nSCALARS ML_NDReynoldsStress double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(ML_features[i,2])+"\t")
	
	f.write("\nSCALARS ML_ShearReynoldsNum double 1")
	f.write("\nLOOKUP_TABLE default\n")
	for i in range(len(features)):
		f.write(str(ML_features[i,1])+"\t")
	
	f.close()
