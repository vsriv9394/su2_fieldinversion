#!bash

#./configure --prefix=$SU2_HOME --enable-PY_WRAPPER --enable-metis --enable-codi-reverse --enable-codi-forward --enable-mpi

#Do this for full build
#export CXXFLAGS="-O3 -Wall"
export CXXFLAGS="-O3 -w"
export SU2_INSTALLPATH="/gpfs/gpfs0/groups/duraisamy/vsriv/bin/SU2"
#export LAPACK_LIB="/usr/lib"
#export LAPACK_INCLUDE="/usr/include"
./preconfigure.py --build=ppc64le-linux-gnu --enable-autodiff --enable-mpi --enable-PY_WRAPPER --prefix=$SU2_INSTALLPATH
#./preconfigure.py --enable-autodiff --enable-mpi --enable-PY_WRAPPER --with-LAPACK-lib=$LAPACK_LIB --with-LAPACK-include=$LAPACK_INCLUDE --prefix=$SU2_INSTALLPATH

#./preconfigure.py --enable-autodiff --enable-mpi --enable-PY_WRAPPER --enable-cgns --prefix=$SU2_INSTALLPATH

